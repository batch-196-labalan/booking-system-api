App: Booking System API
Description: Allows a user to enroll to a course. Allows an admin to CRUD operations. Allows usders to register into our database.


course: 

id,
name - str,
description - str,
price - num,
isActive - boolean
		   default: true




user:

id,
firstName - str,
lastName - str,
email - str,
password - str,
mobileNo. - str,
isAdmin - boolean
		  default: false




enrollment:

id,
course id,
user id, 
status: str,
dateEnrolled - date



---------------


2 way embedding
Associative : enrollment

course: 

id,
name - str,
description - str,
price - num,
isActive - boolean
		   default: true
enrolees: [
	{
		id,
		user id, 
		status: str,
		dateEnrolled - date

	}
]



user:

id,
firstName - str,
lastName - str,
email - str,
password - str,
mobileNo. - str,
isAdmin - boolean
		  default: false
enrollment: [
	{
		id,
		courseid, 
		status: str,
		dateEnrolled - date

	}
]


