/*
The naming convention for model files is the singular and capitalized form of the name of the document
*/

const mongoose = require("mongoose");


/*

	Mongoose Schema

	Before we can create documents from our api to save into our database, we must first determine the structure of the documents to be written in the database.

	Schema acts as a blueprint for our data/document

	A schema is a reperesentation of how the document is structured. It also determines the types of data and the expected properties.

	So, with this, we don't have to worry for if we had input "stock" or "stocks" in our documents, because we can make it uniform with a schema.

	In mongoose, to create a schema, we use the Schema() constructor from mongoose. this will allow us to create a new mongoose schema object.

*/


const courseSchema = new mongoose.Schema({


	name: {
		type:String,
		required: [true, "Name is required"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	price: {
		type: Number,
		required: [true, "Price is required"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	enrolees: [
		{
			userId: {
				type: String,
				required: [true, "User Id is required"]
			},
			dateEnrolled: {
				type: Date,
				default: new Date()
			},
			status: {
				type: String,
				default: "Enrolled"
			}
		}
	]

})


//module.exports - so we can import and use this file in another file.
module.exports = mongoose.model("course",courseSchema);
















