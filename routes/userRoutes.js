
const express = require("express");
const router = express.Router();
const bcrypt = require("bcrypt");
const User = require("../models/User");


//register

router.post('/',(req,res)=>{

    // console.log(req.body);
 const hashedPw = bcrypt.hashSync(req.body.password,10);
 console.log(hashedPw);

 let newUser = new User ({

     firstName: req.body.firstName,
     lastName: req.body.lastName,
     email: req.body.email,
     password: hashedPw,
     mobileNo: req.body.mobileNo

    })

    newUser.save()
    .then(result => res.send(result))
    .catch(error => res.send(error))

  })


router.post('/details',(req,res)=>{

    let userId = req.body._id
    User.find({userId})
    .then(result => res.send(result))
    .catch(error => res.send(error))
});


module.exports = router;