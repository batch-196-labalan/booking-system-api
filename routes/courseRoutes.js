/*
    To be able to create routes from another file to be used in our application, from here, we have to import express as well, however, we will now use another method from express to contain our routes.

    the Router() method, will allow us to contain our routes
*/

const express = require("express");
const router = express.Router();


const Course = require("../models/Course");



router.get('/',(req,res)=>{

    // res.send("This route will get all courses documents");

    Course.find({})
    .then(result => res.send(result))
    .catch(error => res.send(error))
});



router.post('/',(req,res)=>{

    // console.log(req.body);

let newCourse = new Course({
	
	name: req.body.name,
	description: req.body.description,
	price: req.body.price

  })
// console.log(newCourse);

	newCourse.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))

});

module.exports = router;