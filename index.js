const express = require("express");
const mongoose = require("mongoose"); //ODM library to let our expressjs api manipulate a nongodb database. Added after installation of mongoose.

const app = express();
const port = 4000;

/*

Mongoose Connection

mongoose.connect() 

2 arguments:
1. connection string - to connect our api to mongodb
2. objecy - to add info between mongoose and mongodb

replace/change <password> in the connection str to your db user password

just before ? in the connection string, add the database name

*/

mongoose.connect("mongodb+srv://admin:admin123@cluster0.5ed30qq.mongodb.net/bookingAPI?retryWrites=true&w=majority",
{
	useNewUrlParser: true,
	useUnifiedTopology: true

});

let db = mongoose.connection; //create notif if the connection to db was a success

db.on('error',console.error.bind(console, "MongoDB Connection Error."));  //to show notif of an internal server error from a mongoDB

//if the connection is open and succesful, we will output a message in the terminal/gitbash: 

//in terminal type: npm run dev

db.once('open',()=>console.log("Connected to MongoDB."))




app.use(express.json());  


const courseRoutes = require("./routes/courseRoutes");
console.log(courseRoutes);

app.use('/courses',courseRoutes);


//import the user routes
const userRoutes = require("./routes/userRoutes");
app.use('/users',userRoutes);


app.listen(port,()=> console.log('Express API running at port 4000'))




















